trigger MailTrigger on Mail__c (after insert) {

    //When we create a new piece of mail, make a task assigned to the driver letting him know to deliver it
    Mail__c newMail = Trigger.New[0];
    
    //Task basic info
    Task newTask = new Task();
    newTask.Description = 'New Mail to be delivered: ' + newMail.Name;
    
    //Grab the route associated with the mail so we can get its' driver (can't do that with just dot notation)
    //Shouldn't need to limit (id's are unique), but do it just in case
    Route__c assignedRoute = [SELECT id, Driver__c FROM Route__c WHERE id = :newMail.Route__c LIMIT 1];
    newTask.OwnerId = assignedRoute.Driver__c;
    
    insert newTask;
}