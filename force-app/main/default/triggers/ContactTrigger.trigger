trigger ContactTrigger on Contact (before insert) {

    for(Contact c : Trigger.New)
    {    

        //Account Name is a combination of the first and last names
        String fullName = c.FirstName + ' ' + c.LastName;
        List<Account> relatedAccount = [SELECT Id, Name FROM Account WHERE Name LIKE :fullName LIMIT 1];
        //system.debug(relatedAccount);
        //If there is a related account, link the two
        if(relatedAccount.size() != 0)
        {
            c.AccountId = relatedAccount[0].Id;
            //system.debug(c.AccountId + ' is the account ID in the new contact.');
        }

    }
    

    
}