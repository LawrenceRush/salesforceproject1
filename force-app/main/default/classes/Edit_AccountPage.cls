public with sharing class Edit_AccountPage{
   public List<Account> acc{get;set;}
   public Edit_AccountPage(ApexPages.StandardSetController controller1)
    {
        controller1.setPageSize(10);
        acc = new List<Account>();
        acc =  [Select Id,Name,Phone,Rating From Account];
        }
    public PageReference save(){
     update acc;
     return null;
    }
}