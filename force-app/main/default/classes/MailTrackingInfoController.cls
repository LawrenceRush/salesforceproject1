public with sharing class MailTrackingInfoController {
    //Create a holder for the search num and the mail
    public List <Mail__c> trackedMailList {get;set;}
    public List <Address__c> mailAddress {get; set;}
    public Mail__c displayedMail{get;set;}
    public Address__c displayedAddress{get;set;}
    public string searchNum {get;set;} 
    

    public void search(){
    //Query for the mail where the search num is the same as tracking number
	string searchquery='select name, Tracking_Code__c,Destination_Address__c, Delivery_Status__c from Mail__c where Tracking_Code__c =:searchNum Limit 1'; 
    trackedMailList = Database.query(searchquery); 
        if(trackedMailList.size()>0){
            displayedMail = trackedMailList[0];
            id addressID = displayedMail.Destination_Address__c;
            System.Debug(addressID);
            //Find the address with the mail id
           string addressQuery = 'Select Full_Address__c From Address__c Where Id =:addressID';
            mailAddress = Database.query(addressQuery);
            system.debug(mailAddress);
            if (mailAddress.size()>0){
                displayedAddress = mailAddress[0];
            }
        }else if(String.isEmpty(searchNum)){
            displayedMail.clear();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter a tracking number'));
		} else if (trackedMailList.size() == 0){
            displayedMail.clear();
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Not a valid tracking number'));
        }
    }
}