public class SubmitCaseController {
    public Case c { get; set; }
    public SubmitCaseController() {
        c = new Case();
    }
    public PageReference submitCase() {
        //look for an associated contact with the same email
        List<Contact> conList = [SELECT Id FROM Contact WHERE Email 
                                 = :c.SuppliedEmail LIMIT 1];
        

        //if there is a contact, add it to the case, if not, insert case anyway
        if(string.isEmpty(c.Subject)){
              ApexPages.addmessage(new ApexPages.message
                                   (ApexPages.severity.WARNING,'Please enter a Subject'));
	    return null;

        
        }else if(string.isEmpty(c.Description)){
              ApexPages.addmessage(new ApexPages.message
                                   (ApexPages.severity.WARNING,'Please enter a Description'));
            return null;
        } else if (conList.size()>0 ){
            System.debug(conList);
            c.ContactId = conList[0].Id;
            Insert c;        
            return new PageReference('/apex/ThanksForIssue');
        }else{
            Insert c;
            return new PageReference('/apex/ThanksForIssue');
		}
    }
}