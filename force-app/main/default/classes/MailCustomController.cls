public class MailCustomController {

    public ApexPages.StandardSetController setCon {
               get {
            if(setCon == null) {
                
                String userEmail = UserInfo.getUserEmail();
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id, Name, Delivery_Status__c, Tracking_Code__c FROM Mail__c WHERE Recipient__r.Email LIKE :userEmail]));
            }
            return setCon;
        }
        set; 
    }
    
    public List<Mail__c> getMail()
    {
        return (List<Mail__c>) setCon.getRecords();
    }
    
}