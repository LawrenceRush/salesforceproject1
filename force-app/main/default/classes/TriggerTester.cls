@isTest
public class TriggerTester {

    //Testing the Contact Trigger
    @isTest(seeAllData = True)
    public static void ContactTriggerTest()
    {
        /*
        List<Account> debugAccount = [SELECT Id, Name FROM Account WHERE Name LIKE 'Rich%'];
        System.debug(debugAccount[0].Name);
        Contact debugContact = new Contact(FirstName = 'Rich', LastName = 'McRichson');
        System.debug(debugContact.FirstName + ' ' + debugContact.LastName);
        System.assert(debugContact.AccountId == debugAccount[0].id);
		*/
        
        
        //Create an Account to compare to (since the trigger fires before the flow that creates the account)
        Account testAccount = new Account(Name = 'Blah Blahson');
        
        //System.debug(testAccount.Name);
        
        insert testAccount;
        
        //System.debug('Made it out of the first insert!');
        
        //Create a Contact to fire the trigger
		Contact testContact = new Contact(FirstName = 'Blah', LastName = 'Blahson');
        
        //Trigger should fire here
        insert testContact;
        
        System.debug(testAccount.id);
        //The contact variable doesn't seem to update in this context, so we regrab the contact
    	testContact = [SELECT id, AccountId FROM Contact WHERE FirstName = 'Blah'];
        //Are the two related?
        System.assert(testContact.AccountId == testAccount.Id);
	}
    
    //Testing the Mail Trigger
    @isTest(seeAllData = True)
    public static void MailTriggerTest()
    {
        //Grab the first route result for testing
        Route__c testRoute = [SELECT id, Driver__c FROM Route__c LIMIT 1];
        
        //Make a new Mail to fire the trigger
        Mail__c testMail = new Mail__c(Route__c = testRoute.Id);

        insert testMail;
        //Trigger should fire here
        
        //Now to grab the task (grab the newest one, as that's the one the trigger most likely made)
        Task autoCreatedTask = [SELECT id, description, CreatedDate, OwnerId FROM Task ORDER BY CreatedDate DESC LIMIT 1];
        
        //Is the task properly given to the Driver?
        System.assert(autoCreatedTask.OwnerId == testRoute.Driver__c);
        //Did the task description actually get updated?
        System.assert(autoCreatedTask.Description != null);
    }
    
}